# Blender 3DMM Add-on

This is a Blender add-on for the use of 3D Morphable Models within Blender,
made for my Master's Project, supporting loading of 3DMMs, animating them, and
fitting them to images. The add-on supports the following 3DMMs:

- Surrey Face Model (SFM)
- 4D Face Model (4DFM)
- Basel Face Model (BFM) 2009 and 2017
- Liverpool-York Head Model (LYHM)

![Demo of the add-on, showing five renderings of a face morphing from sad to
happy](add-on-demo.png)

### Installation

Unlike most Blender add-ons, this add-on needs a few Python libraries to run
(some of which are only required for some functionality). This means that the
add-on has a slightly more involved installation process --- the PyPI packages
you will need are `eos-py`, `opencv-python` and `dlib` (the latter two are only
necessary for fitting to images). If you know how to do that within Blender's
Python environment, go ahead --- on Linux, it tends to be the system environment.

Otherwise, this short bit of Python code should install the correct libraries
for you, although the installation for `dlib` is disabled by default due to
requiring CMake. If you'd like to fit to images without pre-prepared landmark
files, you'll need to either install CMake and uncomment (remove the `# `) the
line below "only works with cmake installed", or use the dlib-bin PyPI package
by uncommenting the line below "alternative via dlib-bin".

Paste it into a new file in the Scripting tab, run it, and it should do the
work for you. Enabling `Window -> Toggle System Console` is recommended so that
you can see the output. This will also lock up the main Blender interface for a
bit.

```py
import sys
import subprocess

def execute(command):
    subprocess.check_call(command, shell=True, stdout=sys.stdout, stderr=subprocess.STDOUT)

execute([sys.executable, "--version"])
execute([sys.executable, "-m", "ensurepip"])
execute([sys.executable, "-m", "pip", "install", "eos-py", "opencv-python"])

# only works with cmake installed
# execute([sys.executable, "-m", "pip", "install", "dlib"])

# alternative via dlib-bin
# execute([sys.executable, "-m", "pip", "install", "dlib-bin"])
```

After installing the libraries, install the add-on like you would install any
other add-on, and it should work.

### Usage

To load a 3DMM in the add-on, you must first create a 3DMM Object. This is done
in the Add menu (`Add -> 3DMM Object`). Then, navigate to the Object Properties
tab of the Properties area, and find the 3DMM panel. Use the `Model Path` file
picker to load your 3DMM object, in a file format supported by `eos`. You can
get some examples from the [`eos` library's
GitHub](https://github.com/patrikhuber/eos/tree/master/share). For example, you
can load `sfm_shape_3448.bin`.

This will have added at least the shape model to the Object. Some 3DMMs use a
separate file for the expressions model. The Surrey Face Model included in
`eos` does, for example, so it should also be loaded --- use the `Blendshapes
Path` file picker to load this if needed. For example, you can load
`expression_blendshapes_3448.bin`. Certain 3DMMs, like the 4D Face Model, don't
need this step, as they include expressions in the base file.

Once loaded, the functionality of the add-on should be usable. For example, you
can use the Coefficients sub-panels to change the 3DMM. To use the model's
vertex colors, you should create a Material which uses the 3DMM Object's `3DMM
Color` attribute. You can also use the Face Mesh Fitting panel to fit a Face
3DMM to an image of a face. After fitting, a texture will be created, and you
can create a Material which samples from that texture --- the UV Map will
already be set. An example configuration of the required files to achieve this
is shown here:

![A screenshot of Blender, showing how to configure the 3DMM panel to be used
for fitting the Surrey Face Model](fitting-example.png)

All relevant files come from `eos`, besides the image, and `Shape Predictor`
which comes from [the `dlib`
library](http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2). All
files are assumed to be relative to the `.blend` file, in a folder called
`3dmm`.

### Scripting Interface

The add-on has some additional functionality for usage via Python. Most of the
API can be discovered via enabling `Python Tooltips` in the Blender settings
and mousing over UI elements, but there is one Operator not shown in the UI
that may be needed when scripting. When you change the coefficients of a 3DMM
Object, the changes will not immediately be reflected in the mesh. To force the
add-on to apply the changes, you can call `bpy.ops.object.update_mm_object()`,
which will update the context Object. Combining this with
`bpy.context.temp_override` allows you to call it on the object you would like.

Here's an example showing how you can create a 3DMM Object, and then set
the first coefficient to something other than `0.0`, updating it immediately:

```py
import bpy

bpy.ops.object.make_mm_object()
obj = bpy.context.active_object

# change this path to the path for your 3DMM -
# here i've assumed that it's relative to the .blend,
# in a folder called 3dmm
obj.mm_data.model_path = "//3dmm/sfm_shape_3448.bin"
obj.mm_data.shape_coefficients[0].coeff = 1.0
with bpy.context.temp_override(object=obj):
    bpy.ops.object.update_mm_object()
```
