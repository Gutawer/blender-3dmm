import dataclasses
import itertools
from types import NoneType
import typing
import math
import traceback
from dataclasses import dataclass

import bpy  # type: ignore # pylint: disable=import-error
import eos


bl_info = {
    "name": "3DMM Mesh",
    "blender": (3, 5, 0),
    "category": "Object",
}


@dataclass
class ObjectDataEntry:
    last_shape_coeffs: typing.List[float]
    last_color_coeffs: typing.List[float]
    last_expr_coeffs: typing.List[float]


class SessionData:
    def __init__(self):
        self._model_cache: typing.Dict[
            typing.Tuple[str, str], eos.morphablemodel.MorphableModel
        ] = {}
        self._object_data: typing.Dict[int, ObjectDataEntry] = {}

    def get_object_data(self, obj) -> ObjectDataEntry:
        if obj.mm_data.obj_data_index in self._object_data:
            return self._object_data[obj.mm_data.obj_data_index]

        last_shape_coeffs = [math.nan] * len(obj.mm_data.shape_coefficients)
        last_color_coeffs = [math.nan] * len(obj.mm_data.color_coefficients)
        last_expr_coeffs = [math.nan] * len(obj.mm_data.expr_coefficients)
        ret = ObjectDataEntry(last_shape_coeffs, last_color_coeffs, last_expr_coeffs)
        self._object_data[obj.mm_data.obj_data_index] = ret
        return ret

    def get_model_from_paths(self, model_path, blendshapes_path):
        p = (model_path, blendshapes_path)
        if p in self._model_cache:
            return self._model_cache[p]

        model = eos.morphablemodel.load_model(bpy.path.abspath(model_path))
        if blendshapes_path != "":
            blendshapes = eos.morphablemodel.load_blendshapes(
                bpy.path.abspath(blendshapes_path)
            )
            model = eos.morphablemodel.MorphableModel(
                model.get_shape_model(),
                blendshapes,
                model.get_color_model(),
                model.get_landmark_definitions(),
                model.get_texture_coordinates(),
            )
        self._model_cache[p] = model
        return model

    def get_model_from_obj(self, obj) -> eos.morphablemodel.MorphableModel:
        return self.get_model_from_paths(
            obj.mm_data.model_path, obj.mm_data.blendshapes_path
        )

    def prune(self, scene):
        object_indices_dict = {}
        cache_keys_set = set()
        object_keys_set = set()
        for obj in scene.objects:
            if obj.mm_data.is_mm_obj:
                l = object_indices_dict.setdefault(obj.mm_data.obj_data_index, [])
                l.append(obj)
                cache_keys_set.add(
                    (obj.mm_data.model_path, obj.mm_data.blendshapes_path)
                )
                object_keys_set.add(obj.mm_data.obj_data_index)

        for base_index, v in object_indices_dict.items():
            for obj in v[1:]:
                index = scene.next_obj_data_index
                session._object_data[index] = dataclasses.replace(
                    session._object_data[base_index]
                )
                obj.mm_data.obj_data_index = index
                scene.next_obj_data_index += 1

        session._model_cache = {
            k: v for k, v in session._model_cache.items() if k in cache_keys_set
        }
        session._object_data = {
            k: v for k, v in session._object_data.items() if k in object_keys_set
        }

    def clear(self):
        self._model_cache.clear()
        self._object_data.clear()


session = SessionData()


def handle_change_obj(obj):
    mm_data = obj.mm_data

    if mm_data.model_path == "":
        return

    model = session.get_model_from_obj(obj)
    entry = session.get_object_data(obj)
    needs_to_do_shape = False
    needs_to_do_color = False

    new_shape_coefficients = []
    for i, (prop, old) in enumerate(
        zip(mm_data.shape_coefficients, entry.last_shape_coeffs)
    ):
        if prop.coeff != old:
            needs_to_do_shape = True
        entry.last_shape_coeffs[i] = prop.coeff
        new_shape_coefficients.append(prop.coeff)

    new_color_coefficients = []
    for i, (prop, old) in enumerate(
        zip(mm_data.color_coefficients, entry.last_color_coeffs)
    ):
        if prop.coeff != old:
            needs_to_do_color = True
        entry.last_color_coeffs[i] = prop.coeff
        new_color_coefficients.append(prop.coeff)

    new_expr_coefficients = []
    for i, (prop, old) in enumerate(
        zip(mm_data.expr_coefficients, entry.last_expr_coeffs)
    ):
        if prop.coeff != old:
            needs_to_do_shape = True
        entry.last_expr_coeffs[i] = prop.coeff
        new_expr_coefficients.append(prop.coeff)

    if not (needs_to_do_shape or needs_to_do_color):
        return

    if model.has_separate_expression_model():
        sample = model.draw_sample(
            new_shape_coefficients,
            new_expr_coefficients,
            new_color_coefficients,
        )
    else:
        sample = model.draw_sample(new_shape_coefficients, new_color_coefficients)

    mesh = obj.original.data

    if len(mesh.vertices) == len(sample.vertices):
        verts = list(itertools.chain(*sample.vertices))
        mesh.vertices.foreach_set("co", verts)
    else:
        vertices = sample.vertices
        edges = []
        faces = sample.tvi
        mesh.clear_geometry()
        mesh.from_pydata(vertices, edges, faces)
        colattr = mesh.color_attributes.new(
            name="3DMM Color",
            type="FLOAT_COLOR",
            domain="POINT",
        )
        needs_to_do_color = True

    if needs_to_do_color:
        colattr = mesh.color_attributes.get("3DMM Color")
        cols = list(
            itertools.chain(*map(lambda col: itertools.chain(col, [1]), sample.colors))
        )
        colattr.data.foreach_set("color", cols)

    mesh.update()


@bpy.app.handlers.persistent
def handle_change(scene, depsgraph):
    session.prune(scene)

    for update in depsgraph.updates:
        if hasattr(update.id, "mm_data") and update.id.mm_data.is_mm_obj:
            handle_change_obj(update.id)


@bpy.app.handlers.persistent
def handle_load(__dummy__):
    session.clear()


bpy.app.handlers.load_post.append(handle_load)


class MakeMmObjectOperator(bpy.types.Operator):
    bl_idname = "object.make_mm_object"
    bl_label = "3DMM Object"
    bl_description = "Construct a mesh which uses a 3D Morphable Model"

    def execute(self, __context__):
        new_mesh = bpy.data.meshes.new("3DMM Mesh")
        new_object = bpy.data.objects.new("3DMM", new_mesh)
        new_object.mm_data.is_mm_obj = True
        bpy.data.collections["Collection"].objects.link(new_object)
        bpy.context.view_layer.objects.active = new_object
        return {"FINISHED"}


fail_type = ""
fail_path = ""
fail_reason = ""


def oops(self, __context__):
    self.layout.label(text=f"Invalid path for loading 3DMM {fail_type}: {fail_path}")
    self.layout.label(text="")
    for l in fail_reason.splitlines():
        self.layout.label(text=l)


def add_model_path(__self__, context):
    global fail_type, fail_path, fail_reason

    obj = context.object

    if (
        not obj.mm_data.is_mm_obj
        or obj.mm_data.model_path == ""
        or len(obj.mm_data.shape_coefficients) != 0
    ):
        return

    try:
        model = session.get_model_from_paths(obj.mm_data.model_path, "")
    except Exception:
        fail_type = "model"
        fail_path = obj.mm_data.model_path
        fail_reason = traceback.format_exc()
        bpy.context.window_manager.popup_menu(oops, title="Error", icon="ERROR")
        obj.mm_data.model_path = ""
        raise

    shape_count = model.get_shape_model().get_num_principal_components()
    color_count = model.get_color_model().get_num_principal_components()
    expr_count = 0
    sample = None
    if model.has_separate_expression_model():
        expr_model = model.get_expression_model()
        if isinstance(expr_model, list):
            expr_count = len(expr_model)
        elif not isinstance(expr_model, NoneType):
            expr_count = expr_model.get_num_principal_components()
        sample = model.draw_sample(
            [0] * shape_count, [0] * expr_count, [0] * color_count
        )
    else:
        sample = model.draw_sample([0] * shape_count, [0] * color_count)

    vertices = sample.vertices
    edges = []
    faces = sample.tvi
    obj.data.from_pydata(vertices, edges, faces)

    colattr = obj.data.color_attributes.new(
        name="3DMM Color",
        type="FLOAT_COLOR",
        domain="POINT",
    )
    cols = list(
        itertools.chain(*map(lambda col: itertools.chain(col, [1]), sample.colors))
    )
    colattr.data.foreach_set("color", cols)

    obj.data.update()

    for _ in range(shape_count):
        item = obj.mm_data.shape_coefficients.add()
        item.coeff = 0.0
    for _ in range(color_count):
        item = obj.mm_data.color_coefficients.add()
        item.coeff = 0.0
    for _ in range(expr_count):
        item = obj.mm_data.expr_coefficients.add()
        item.coeff = 0.0

    verts = model.get_texture_coordinates()

    layer = obj.data.uv_layers.new(name="UV")
    obj.data.uv_layers.active = layer
    for loop in obj.data.loops:
        obj.data.uv_layers.active.data[loop.index].uv = (
            verts[loop.vertex_index][0],
            1.0 - verts[loop.vertex_index][1],
        )


def add_blendshapes_path(__self__, context):
    global fail_type, fail_path, fail_reason

    obj = context.object

    if (
        not obj.mm_data.is_mm_obj
        or obj.mm_data.model_path == ""
        or obj.mm_data.blendshapes_path == ""
        or len(obj.mm_data.expr_coefficients) != 0
    ):
        return

    entry = session.get_object_data(obj)
    try:
        blendshapes = eos.morphablemodel.load_blendshapes(
            bpy.path.abspath(obj.mm_data.blendshapes_path)
        )
    except Exception:
        fail_type = "blendshapes"
        fail_path = obj.mm_data.blendshapes_path
        fail_reason = traceback.format_exc()
        bpy.context.window_manager.popup_menu(oops, title="Error", icon="ERROR")
        obj.mm_data.blendshapes_path = ""
        raise

    expr_count = len(blendshapes)
    for _ in range(expr_count):
        item = obj.mm_data.expr_coefficients.add()
        item.coeff = 0.0
    entry.last_expr_coeffs = [0] * expr_count


class DoFittingOperator(bpy.types.Operator):
    bl_idname = "object.face_mesh_do_fitting"
    bl_label = "Perform Fitting"
    bl_description = (
        "Perform fitting of a 3DMM Object to a face image using "
        + "parameters set in the 3DMM Face Mesh Fitting panel"
    )

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj is not None and obj.mm_data.is_mm_obj

    def execute(self, context):
        import numpy as np
        import cv2

        obj = context.object

        if not obj.mm_data.is_mm_obj:
            self.report({"ERROR"}, "Object must be a 3DMM object")

        image = cv2.imread(bpy.path.abspath(obj.mm_data.image_path))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2BGRA)

        landmarks = None
        if obj.mm_data.pts_path != "":
            landmarks = read_pts(bpy.path.abspath(obj.mm_data.pts_path))
        else:
            import dlib

            dlib_img = cv2.cvtColor(image, cv2.COLOR_BGRA2RGB)

            detector = dlib.get_frontal_face_detector()  # type: ignore
            sp = dlib.shape_predictor(  # type: ignore
                bpy.path.abspath(obj.mm_data.shape_predictor_path)
            )
            rects = detector(dlib_img)
            rect = rects[0]  # TODO: multi-face support would be nice
            shape = sp(dlib_img, rect)
            landmarks = []
            for i, p in enumerate(shape.parts()):
                landmarks.append(eos.core.Landmark(str(i + 1), [float(p.x), float(p.y)]))  # type: ignore

        landmark_mapper = eos.core.LandmarkMapper(
            bpy.path.abspath(obj.mm_data.mappings_file_path)
        )
        edge_topology = eos.morphablemodel.load_edge_topology(
            bpy.path.abspath(obj.mm_data.edge_topology_path)
        )
        contour_landmarks = eos.fitting.ContourLandmarks.load(
            bpy.path.abspath(obj.mm_data.mappings_file_path)
        )
        model_contour = (
            eos.fitting.ModelContour.load(
                bpy.path.abspath(obj.mm_data.model_contour_path)
            )
            if obj.mm_data.model_contour_path != ""
            else eos.fitting.ModelContour()
        )

        model = session.get_model_from_obj(obj)

        image_height, image_width, _ = image.shape

        (mesh, pose, shape_coeffs, expr_coeffs) = eos.fitting.fit_shape_and_pose(
            model,
            landmarks,
            landmark_mapper,
            image_width,
            image_height,
            edge_topology,
            contour_landmarks,
            model_contour,
        )

        for prop, c in zip(obj.mm_data.shape_coefficients, shape_coeffs):
            prop.coeff = c

        for prop, c in zip(obj.mm_data.expr_coefficients, expr_coeffs):
            prop.coeff = c

        texture_map = eos.render.extract_texture(
            mesh, pose, image, texturemap_resolution=2048
        )
        texture_map = cv2.flip(texture_map, 0)
        texture_map = texture_map.astype(np.float32)
        texture_map /= 255.0

        name = obj.name + "_FittedTexture"
        bpy_image = (
            bpy.data.images.new(
                name, width=texture_map.shape[1], height=texture_map.shape[0]
            )
            if name not in bpy.data.images
            else bpy.data.images[name]
        )
        bpy_image.scale(texture_map.shape[1], texture_map.shape[0])
        bpy_image.pixels[:] = cv2.cvtColor(texture_map, cv2.COLOR_BGRA2RGBA).flatten()

        handle_change_obj(obj)

        return {"FINISHED"}


def read_pts(filename):
    """A helper function to read the 68 ibug landmarks from a .pts file."""
    lines = open(filename).read().splitlines()
    lines = lines[3:71]

    landmarks = []
    ibug_index = 1  # count from 1 to 68 for all ibug landmarks
    for l in lines:
        coords = l.split()
        landmarks.append(
            eos.core.Landmark(str(ibug_index), [float(coords[0]), float(coords[1])])  # type: ignore
        )
        ibug_index = ibug_index + 1

    return landmarks


class UpdateMmObjectOperator(bpy.types.Operator):
    bl_idname = "object.update_mm_object"
    bl_label = "Update 3DMM"
    bl_description = "Update a 3DMM Object, setting its geometry - intended for use in Python scripting"

    def execute(self, context):
        if not context.object.mm_data.is_mm_obj:
            return {"CANCELLED"}

        handle_change_obj(context.object)
        return {"FINISHED"}


operators = [MakeMmObjectOperator, DoFittingOperator, UpdateMmObjectOperator]

MEAN_OPERATOR_DATA = {
    "shape": {
        "label": "Mean Shape Coefficients",
        "idname": "object.mm_mesh_shape_mean",
        "description": (
            "Set the shape coefficients to all be 0, "
            + "which sets the 3DMM shape to its mean"
        ),
        "attr": "shape_coefficients",
    },
    "color": {
        "label": "Mean Color Coefficients",
        "idname": "object.mm_mesh_color_mean",
        "description": (
            "Set the color coefficients to all be 0, "
            + "which sets the 3DMM color to its mean"
        ),
        "attr": "color_coefficients",
    },
    "expr": {
        "label": "Mean Expression Coefficients",
        "idname": "object.mm_mesh_expr_mean",
        "description": (
            "Set the expression coefficients to all be 0, "
            + "which sets the 3DMM expression to its mean"
        ),
        "attr": "expr_coefficients",
    },
}


def add_mean_operator_data():
    for data in MEAN_OPERATOR_DATA.values():

        def make_class(data):
            class Operator(bpy.types.Operator):
                bl_idname = data["idname"]
                bl_label = data["label"]
                bl_description = data["description"]

                @classmethod
                def poll(cls, context):
                    obj = context.object
                    return (
                        obj is not None
                        and obj.mm_data.is_mm_obj
                        and len(getattr(obj.mm_data, data["attr"])) != 0
                    )

                def execute(self, context):
                    obj = context.object

                    if not obj.mm_data.is_mm_obj:
                        self.report({"ERROR"}, "Object must be a 3DMM object")

                    for prop in getattr(obj.mm_data, data["attr"]):
                        prop.coeff = 0.0

                    handle_change_obj(obj)

                    return {"FINISHED"}

            return Operator

        operators.append(make_class(data))


add_mean_operator_data()


# pylint: disable=too-few-public-methods
class CoeffPropertyGroup(bpy.types.PropertyGroup):
    coeff: bpy.props.FloatProperty(
        description="A coefficient in a 3DMM Object"
    )  # type: ignore


bpy.utils.register_class(CoeffPropertyGroup)


# pylint: disable=too-few-public-methods
class MmPropertyGroup(bpy.types.PropertyGroup):
    is_mm_obj: bpy.props.BoolProperty()  # type: ignore
    obj_data_index: bpy.props.IntProperty()  # type: ignore
    model_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        update=add_model_path,
        name="3DMM Model Path",
        description="Path for the 3DMM",
    )  # type: ignore
    blendshapes_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        update=add_blendshapes_path,
        name="Blendshapes Path",
        description="Path for an separate blendshapes expression model",
    )  # type: ignore

    shape_coefficients: bpy.props.CollectionProperty(
        type=CoeffPropertyGroup,
        name="Shape Coefficients",
        description="The coefficients for the shape model",
    )  # type: ignore
    color_coefficients: bpy.props.CollectionProperty(
        type=CoeffPropertyGroup,
        name="Color Coefficients",
        description="The coefficients for the color model",
    )  # type: ignore
    expr_coefficients: bpy.props.CollectionProperty(
        type=CoeffPropertyGroup,
        name="Expression Coefficients",
        description="The coefficients for the expression model",
    )  # type: ignore

    mappings_file_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Mappings File Path",
        description="Path for a mappings file from ibug landmarks to the chosen model - "
        + "e.g. ibug_to_sfm.txt",
    )  # type: ignore
    edge_topology_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Edge Topology Path",
        description="Path for an edge topology file",
    )  # type: ignore
    model_contour_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Model Contour Path",
        description="Path for a model contour file",
    )  # type: ignore
    image_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Image Path",
        description="Path for the image used for fitting",
    )  # type: ignore
    shape_predictor_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Shape Predictor Path",
        description="Path for a dlib shape predictor to predict landmarks from the image, "
        + "mutually exclusive with Points Path",
    )  # type: ignore
    pts_path: bpy.props.StringProperty(
        subtype="FILE_PATH",
        name="3DMM Points Path",
        description="Path for a points file containing landmarks for the image, "
        + "mutually exclusive with Shape Predictor Path",
    )  # type: ignore


bpy.utils.register_class(MmPropertyGroup)
bpy.types.Object.mm_data = bpy.props.PointerProperty(type=MmPropertyGroup)
bpy.types.Scene.next_obj_data_index = bpy.props.IntProperty()


class MmMeshPanel(bpy.types.Panel):
    bl_label = "3DMM"
    bl_idname = "OBJECT_PT_mm_mesh_meta"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    @classmethod
    def poll(cls, context):
        obj = context.object
        return obj is not None and obj.mm_data.is_mm_obj

    def draw(self, context):
        layout = self.layout
        obj = context.object
        if not obj.mm_data.is_mm_obj:
            return

        row = layout.row()
        row.prop(obj.mm_data, "model_path", text="Model Path")
        if obj.mm_data.model_path != "":
            row.enabled = False

        if obj.mm_data.model_path != "" and (
            obj.mm_data.blendshapes_path != ""
            or not session.get_model_from_obj(obj).has_separate_expression_model()
        ):
            row = layout.row()
            row.prop(obj.mm_data, "blendshapes_path", text="Blendshapes Path")
            if (
                obj.mm_data.blendshapes_path != ""
                or session.get_model_from_obj(obj).has_separate_expression_model()
            ):
                row.enabled = False


FITTING_PATH_DATA = {
    "mappings_file_path": {"name": "Mappings File Path", "mutually_exclusive": []},
    "edge_topology_path": {"name": "Edge Topology Path", "mutually_exclusive": []},
    "model_contour_path": {"name": "Model Contour Path", "mutually_exclusive": []},
    "image_path": {"name": "Image Path", "mutually_exclusive": []},
    "shape_predictor_path": {
        "name": "Shape Predictor Path",
        "mutually_exclusive": ["pts_path"],
    },
    "pts_path": {"name": "Points Path", "mutually_exclusive": ["shape_predictor_path"]},
}


class FittingPanel(bpy.types.Panel):
    bl_label = "Face Mesh Fitting"
    bl_parent_id = "OBJECT_PT_mm_mesh_meta"
    bl_idname = "OBJECT_PT_mm_face_mesh_fitting"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_options = {"DEFAULT_CLOSED"}

    @classmethod
    def poll(cls, context):
        obj = context.object
        return (
            obj is not None and obj.mm_data.is_mm_obj and obj.mm_data.model_path != ""
        )

    def draw(self, context):
        layout = self.layout
        obj = context.object
        if not obj.mm_data.is_mm_obj:
            return

        for attr, data in FITTING_PATH_DATA.items():
            row = layout.row()
            row.prop(obj.mm_data, attr, text=data["name"])
            for m in data["mutually_exclusive"]:
                if getattr(obj.mm_data, m) != "":
                    row.enabled = False

        layout.operator(DoFittingOperator.bl_idname, text="Perform Fitting")


panels = [MmMeshPanel, FittingPanel]

PANEL_DATA = {
    "shape": {
        "label": "Shape Coefficients",
        "idname": "OBJECT_PT_mm_mesh_shape",
        "attr": "shape_coefficients",
        "key": "shape",
        "long": "Shape",
        "short": "Shape",
    },
    "color": {
        "label": "Color Coefficients",
        "idname": "OBJECT_PT_mm_mesh_color",
        "attr": "color_coefficients",
        "key": "color",
        "long": "Color",
        "short": "Color",
    },
    "expr": {
        "label": "Expression Coefficients",
        "idname": "OBJECT_PT_mm_mesh_expr",
        "attr": "expr_coefficients",
        "key": "expr",
        "long": "Expression",
        "short": "Expr",
    },
}


def add_panel_data():
    for data in PANEL_DATA.values():

        def make_class(data):
            class Panel(bpy.types.Panel):
                bl_label = data["label"]
                bl_parent_id = "OBJECT_PT_mm_mesh_meta"
                bl_idname = data["idname"]
                bl_space_type = "PROPERTIES"
                bl_region_type = "WINDOW"
                bl_context = "object"
                bl_options = {"DEFAULT_CLOSED"}

                @classmethod
                def poll(cls, context):
                    obj = context.object
                    return (
                        obj is not None
                        and obj.mm_data.is_mm_obj
                        and len(getattr(obj.mm_data, data["attr"])) != 0
                    )

                def draw(self, context):
                    layout = self.layout

                    obj = context.object

                    if obj is not None and obj.mm_data.is_mm_obj:
                        if len(getattr(obj.mm_data, data["attr"])) != 0:
                            layout.operator(
                                MEAN_OPERATOR_DATA[data["key"]]["idname"],
                                text=f"Set Mean {data['long']}",
                            )

                        for i, prop in enumerate(getattr(obj.mm_data, data["attr"])):
                            layout.prop(
                                prop, "coeff", text=f"{data['short']} Coeff {i}"
                            )

            return Panel

        panels.insert(-1, make_class(data))


add_panel_data()


def draw_add_item(self, __context__):
    self.layout.separator()
    self.layout.operator("object.make_mm_object", icon="PLUGIN")


def register():
    for oper in operators:
        bpy.utils.register_class(oper)
    for panel in panels:
        bpy.utils.register_class(panel)
    bpy.types.VIEW3D_MT_add.append(draw_add_item)
    bpy.app.handlers.frame_change_post.append(handle_change)
    bpy.app.handlers.depsgraph_update_post.append(handle_change)


def unregister():
    bpy.app.handlers.frame_change_post.remove(handle_change)
    bpy.app.handlers.depsgraph_update_post.remove(handle_change)
    bpy.types.VIEW3D_MT_add.remove(draw_add_item)
    for panel in reversed(panels):
        if "bl_rna" in panel.__dict__:
            bpy.utils.unregister_class(panel)
    for oper in reversed(operators):
        if "bl_rna" in oper.__dict__:
            bpy.utils.unregister_class(oper)
    bpy.utils.unregister_class(MmPropertyGroup)
    bpy.utils.unregister_class(CoeffPropertyGroup)


if __name__ == "__main__":
    register()
